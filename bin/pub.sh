#!/bin/bash

. DANKU_VERSION


echo "******************* v$DANKU_VERSION *****************"

source ../bin/cordova-pub.sh

cd dist
#cd toto
tar czf /tmp/danku.tgz --no-xattrs *
cd ..



scp /tmp/danku.tgz danku@danku:/tmp
#ssh  danku@danku "rm -rf  /var/www/canardoux.xyz/danku/danku/react-io/*; tar xzf /tmp/danku.tgz -C /var/www/canardoux.xyz/danku/danku-react-io; rm /tmp/danku.tgz"
ssh  danku@danku "rm -rf /var/www/canardoux.xyz/danku/live/*   /var/www/canardoux.xyz/danku/danku/react-io/*; tar xzf /tmp/danku.tgz -C /var/www/canardoux.xyz/danku/live; tar xzf /tmp/danku.tgz -C /var/www/canardoux.xyz/danku/danku-react-io; rm /tmp/danku.tgz"

#cd platforms/browser/www
#tar cz  --no-xattrs  -f ../../../_toto.tgz *
#cd ../../..
#scp _toto.tgz danku@danku:~
#ssh  danku@danku "rm -rf /var/www/canardoux.xyz/danku/live/*; tar xzf _toto.tgz -C /var/www/canardoux.xyz/danku/live; rm _toto.tgz"
#rm _toto.tgz




git add .
git commit -m "Danku-svelte : Version $DANKU_VERSION"
git pull origin
git push origin
#if [ ! -z "$DANKU_VERSION" ]; then
    git tag -f $DANKU_VERSION
    git push  -f origin $DANKU_VERSION
#fi
